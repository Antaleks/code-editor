(function (){
  

  // загружаем код из стор
  document.addEventListener('DOMContentLoaded', loadCode);

  // сохраняем код при каждом изменении
  document.addEventListener('keyup', saveCode);
  // document.addEventListener('keyup', formatCode);hljs.highlightAll();
  // document.addEventListener('keyup', hljs.highlightAll());
})();

// загрузка кода
function loadCode(){
  // установить <br> для перевода строки (див по умолчанию) 
  document.execCommand("defaultParagraphSeparator", false, "br");
  //
  if (localStorage.getItem('text_in_editor') !== null) {
    document.getElementById('editor').innerHTML = localStorage.getItem('text_in_editor');
  }
}
// сохранение кода
function saveCode (e) {
  localStorage.setItem('text_in_editor', document.getElementById('editor').innerHTML);
}
// format code - add color scheme, ...
function formatCode(e) {
  let strList = new Array();
  let edt = document.querySelector("#editor");
  // получаем строки
  edt.childNodes.forEach(node => {
    // цикл по словам
    KeyWords.forEach(word => {
      // проверить строку на наличие слов из массива
      let inh = node.innerHTML 
      // если находим слово - 
      if (inh.includes(word)){

        // если неотформатировано - 
        
        // то добавляем класс
        let finh = inh.replace(word, `<span class="kw">${word}</span>`)
        node.innerHTML = finh

        // если слов нет класс удаляем
      }
    })
  });
}


function runProgram () {
  
  let editor = document.querySelector('#editor');
/*
  todo после подсветки все строки собираются в одну строку - разобраться
  div <-> br - br использовать как разделитель строки ???

  then highlight
  hljs.highlightElement(editor);

  todo после подсветки все строки не лежат в <div> блоке, поєтому перебор строк по
  дочерним нодам не нужет, нужен textContetnt
*/

  let strList = new Array();
  // получаем чистые строки кода (без тегов)
  editor.childNodes.forEach(node => strList.push(node.textContent));
  // собираем строки в программу с ; в конце строк
  let program = ""
  strList.forEach(str => {
    // todo исключить нежелательные и опасные слова типа querySelector, getElement.., document ...
    if (str.length > 0) program += (str + (str.endsWith(";") ? "" : ";"));
  });
  // определяем "свои" функции print(), input(), ...
  let myCode = "function print(val){alert(val);}\n";
  // собирем код вместе
  program += myCode;
  // запускаем программу
  eval(program);
}


//#key words
let KeyWords = new Array(
  'break',
  'case',
  'class',
  'catch',
  'const',
  'continue',
  'debugger',
  'default',
  'delete',
  'do',
  'else',
  'export',
  'extends',
  'finally',
  'for',
  'function',
  'if',
  'import',
  'in',
  'instanceof',
  'let',
  'new',
  'return',
  'super',
  'switch',
  'this',
  'throw',
  'try',
  'typeof',
  'var',
  'void',
  'while',
  'with',
  'yield',
  'enum',
  'wait',
  'implements',
  'package',
  'protected',
  'static',
  'interface',
  'private',
  'public',
  'abstract',
  'boolean',
  'byte',
  'char',
  'double',
  'final',
  'float',
  'goto',
  'int',
  'long',
  'native',
  'short',
  'synchronized',
  'transient',
  'volatile'
)

//   #literal
let LitWords = Array(
  'null',
  'true',
  'false'
) 